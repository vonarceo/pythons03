class Camper():
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Currently enrolled in the {self.course_type}")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")

student = Camper("Von Arceo", "B210FT", "Python Short Course")

print(f"Camper Name: {student.name}")
print(f"Camper Batch: {student.batch}")
print(f"Camper Course: {student.course_type}")

student.info()
student.career_track()        